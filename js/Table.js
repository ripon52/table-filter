var table='';

var i,j,rowIndex,td,tdText,totalRow,table_id;

class Table{

    load(){
        jQuery.ajax({
            method:"get",
            url:"https://restcountries.eu/rest/v2/all",
            dataType:"json",
            success:function(res){
                this.totalRow = res.length;

                (res).forEach((data,index)=>{

                    var row = ' <tr>'
                                 +'<td>'+data.name+'</td>'
                                 +'<td>'+data.capital+'</td>'
                                 +'<td>'+data.region+'</td>'
                                 +'<td>'+data.population+'</td>'
                                 +'<td>'+data.area+'</td>'
                                 +'<td>'+data.timezones[0]+'</td>'
                                 +'<td>'+data.nativeName+'</td>'
                                +'</tr> ';
                    $("#example tbody").append(row);
                });
            }
        });

    }


    tableRead(typing){
        var tr = $("#example tbody tr");
        var order = $(".orderBy").val();

        if(typing == ''){
            $('#example tbody tr td').css({"background":"#fff","color":"#000"});
        }else{
                    
            for( i = 0;i<tr.length;i++){

                 td = tr[i].getElementsByTagName('td');

                if(i<order){
                    rowIndex = null;

                    for(j=0;j<td.length;j++){

                        if(td[j]){

                            tdText = td[j].innerText.toUpperCase() || td[j].textContent.toUpperCase();

                            if (tdText.indexOf(typing) > -1) 
                            {
                                rowIndex = i;
                                td[j].style.background="#34495e";
                                td[j].style.color="#fff";
                            }else{
                                td[j].style.background="#fff";
                                td[j].style.color="#000";
                            }
                        }
                    }
                    if(rowIndex !=null){
                        tr[rowIndex].style.display='';
                    }else{
                        tr[i].style.display='none';
                    }
                }else{
                    tr[i].style.display='none';
                }
               
            }   

        }
    
    }

}